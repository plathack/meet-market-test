<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('imageable_id')->nullable(false);
            $table->string('imageable_type')->nullable(false);
            $table->string('filename')->nullable(false);
            $table->string('path')->nullable(false);
            $table->boolean('from_url')->default(false);
            $table->string('disk')->default('public');
            $table->string('alt');
            $table->string('title');
            $table->string('scope');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images');
    }
};
